package com.subhankar.task2.ui

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.TextView
import androidx.lifecycle.Observer
import com.subhankar.task2.MyOrientationService
import com.subhankar.task2.R
import com.subhankar.task2.sdk.IOrientationAidlInterface

class MainActivity : AppCompatActivity() {


    private var iOrientationAidlInterface: IOrientationAidlInterface? = null
    private var orientationServiceConnection: ServiceConnection? = null
    private var orientationServiceIntent: Intent? = null
    private lateinit var tvSensorData: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {

        tvSensorData = findViewById(R.id.tv_sensor_data)

        // receives live data to populate in ui
        MyOrientationService.sensorData.observe(this, Observer {
            tvSensorData.text = it.contentToString()
        })


        // executes when screen is rotated
        iOrientationAidlInterface?.orientation()?.let {
            tvSensorData.text = it
        }



        orientationServiceIntent = Intent(this, MyOrientationService::class.java)
        orientationServiceConnection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName?, binder: IBinder?) {
                iOrientationAidlInterface = IOrientationAidlInterface.Stub.asInterface(binder)

                iOrientationAidlInterface?.orientation()?.let {
                    tvSensorData.text = it // updates ui
                }
            }

            override fun onServiceDisconnected(componentName: ComponentName?) {
            }
        }
        orientationServiceIntent?.let {
            orientationServiceConnection?.let {
                bindService(
                    orientationServiceIntent,
                    it,
                    Context.BIND_AUTO_CREATE
                )
            }
        }
    }

}