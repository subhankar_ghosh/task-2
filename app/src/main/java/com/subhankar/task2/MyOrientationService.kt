package com.subhankar.task2

import android.app.Service
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.IBinder
import android.os.RemoteCallbackList
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.MutableLiveData
import com.subhankar.task2.sdk.IMyAidlCbInterface
import com.subhankar.task2.sdk.IOrientationAidlInterface


private const val SENSOR_DELAY_MILLI = 8*1000 //8ms
class MyOrientationService : Service(), SensorEventListener {

    private var sensorManager: SensorManager? = null
    private var rotationSensor: Sensor? = null
    private var mCbs: RemoteCallbackList<IMyAidlCbInterface> = RemoteCallbackList<IMyAidlCbInterface>()
    companion object {
        val sensorData = MutableLiveData<FloatArray>()
    }

    override fun onSensorChanged(event: SensorEvent?) {
        event?.let {
            mCbs.beginBroadcast()
            if (it.sensor.type == Sensor.TYPE_ROTATION_VECTOR) {
                sensorData.value = it.values
                println("@@@ "+mCbs.registeredCallbackCount)
                if(mCbs.registeredCallbackCount>0) {
                    val cb: IMyAidlCbInterface = mCbs.getBroadcastItem(0)
                    cb.showData(it.values.contentToString())
                }
            }
            mCbs.finishBroadcast()
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    private fun createSensorManager() {
        if (sensorManager == null) {
            sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
            rotationSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
            rotationSensor?.let {
                addRotationSensorListener()
            }
        }
    }


    private fun addRotationSensorListener() {
        sensorManager?.registerListener(
            this,
            rotationSensor,
            SENSOR_DELAY_MILLI
        )
    }


    private val myBinder = object : IOrientationAidlInterface.Stub() {
        override fun orientation(): String {
            createSensorManager()
            return sensorData.value?.contentToString() ?: "Waiting for sensor value"
        }
        override fun registerCb(cb: IMyAidlCbInterface?) {
            println("@@@ mCbs.register")
            mCbs.register(cb);
        }

        override fun unRegisterCb(cb: IMyAidlCbInterface?) {
            println("@@@ mCbs.unregister")
            mCbs.unregister(cb);
        }

    }

    override fun onBind(intent: Intent?): IBinder? {
//        super.onBind(intent)
        return myBinder
    }
}