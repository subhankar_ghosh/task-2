// IMyAidlCbInterface.aidl
package com.subhankar.task2.sdk;

// Declare any non-default types here with import statements

interface IMyAidlCbInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void showData(String data);
}